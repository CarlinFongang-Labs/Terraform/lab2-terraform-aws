# Terrarm | Déploiement de ressources AWS avec Terraform

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Définition
>![alt text](img/image-19.png)

## Contexte
L'objectif de ce lab sera de :

1. Accéder et récupérer l'access key du compte AWS qui a été créé.

2. Créer une paire de clés pour l'instance EC2 (key_pair.pem).

3. Créer un fichier ec2.tf, qui est le manifest permettant de provisionner nos nouvelles instances EC2.

4. Dans le ec2.tf, nous allons renseigner les informations permettant de créer une VM avec l'image Ubuntu 22 : Ubuntu Server 22.04 LTS (HVM), SSD Volume Type (ami-0c7217cdde317cfec).

5. Ceci se passera dans la région US East (N. Virginia) us-east-1.

6. Vérifier que l'instance est bien créée et afficher le contenu du fichier tfstate.

7. Modifier le fichier ec2.tf afin d'y inclure le tag de l'instance "Name: ec2-<tag>".

8. Afficher les changements ainsi que dans le fichier tfstate.

9. Pour finir, nous supprimerons l'instance EC2.

## Prerequis
Créer un compte AWS [lab : création d'un compte aws](https://gitlab.com/CarlinFongang-Labs/Terraform/lab0-sigup-aws)
Installation de Terraform [lab : installation de terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab1-intall-terraform)


## Récupération de l'Access key et Secret Access Key depuis la console aws
1. Se connecter à la console AWS
>![alt text](image.png)

2. Accéder au services **"IAM"** depuis la recherche sur la console AWS
>![alt text](img/image-1.png)

3. Sélectionner **"User"** dans le volet droit
>![alt text](img/image-2.png)

4. Sélectionner l'**user** dont on veut récupérer les accès key
>![alt text](img/image-3.png)

5. Se rendre dans le volet **"Security credentials"**
>![alt text](img/image-4.png)

6. Se rendre dans la catégorie **"Access keys"** 
>![alt text](img/image-5.png)

7. Créer un "Access key" en cliquant sur le bouton **"Create access key"**
Selectionner CLI comme cas d'utilisation

>![alt text](img/image-7.png)

8. Rajouter une description 
rajouter une description qui facilitera l'identification et l'usage de la clé et valider 
>![alt text](img/image-8.png)

9. Sauvegarde des information de clé d'access
Une fois les clés d'access généré, les copier et sauvegarder 
Télécharger aussi le ces identifiants en cliquant sur **"Download .csv file"**
>![alt text](img/image-9.png)

10. Une fois les "Acces keys" généré, on est redirigé vers le tableau de bord d'administration User
L'on peut à présent voir les Access keys générés
>![alt text](img/image-10.png)


## Création d'une paire de clé ssh dépuis la console AWS
Une fois connecter au compte AWS

1. Entrer ec2 dans la barre de recherche de la console AWS et y accéder
>![alt text](img/image-11.png)

2. Accéder au menu **"Key pairs"** dans le menu de droite
>![alt text](img/image-12.png)

3. Créer une nouvelle paire de clé en cliquant sur **"Create key pair"**
>![alt text](img/image-13.png)

4. Entrer les informations pour la création de la paire de clé ssh
Une fois les informations renseigner, cliquer sur **"Create key pair"**
>![alt text](img/image-14.png)

5. Un fois la paire de clé crée, la sauvegarder au format .pem dans un emplacement de la machine locale
>![alt text](img/image-15.png)

6. La création de la paire de clé achevé, l'on est redirigé vers la page de listing des paires de clé ssh
>![alt text](img/image-16.png)

Notre paire de clé ssh est bien crée et sauvegarder

## Création du fichier ec2.tf

1. Créer un nouveau fichier dans VSCode nommé ec2.tf
File > New file > "Enter the name of new file and save"

>![alt text](img/image-17.png)

2. Définition du provider : aws
on peut observer l'autocoplétion fournir par le plugin installé [lab : plugin Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab0-sigup-aws#2-installation-du-plugin-terraform-pour-vscode)

>![alt text](img/image-18.png)


Ce code Terraform configure le fournisseur AWS avec les informations d'identification nécessaires pour accéder à la région us-east-1.

````bash
provider "aws" {
    region = "us-east-1"
    access_key = "AKIAZ24ILOXXNY6TCBFB"
    secret_key = "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"

}
````
3. Définition des ressources à provisionner

Ce code Terraform crée une instance EC2 sur AWS avec l'AMI spécifié, le type d'instance et la clé SSH associée.

````bash
resource "aws_instance" "ec2-tf" {
    ami = "ami-0c7217cdde317cfec"
    instance_type = "t3.medium"
    key_name = "devops-digital"  
}
````

4. Initialistion du code terraform
````bash
terraform init
````

téléchargement des plugin utile au provisionnement sur aws
>![alt text](img/image-20.png)


5. Terraform plan
La commande Terraform `plan` génère un plan d'exécution pour les modifications d'infrastructure.
terraform plan
````bash
terraform plan
````

>![alt text](img/image-21.png)
>![alt text](img/image-22.png)

6. Application du plan de provisionnement des ressources

````bash
terraform apply
````

>![alt text](img/image-23.png)
*Résultat de l'entrée de la commande **terraform apply***

une message de confirmation de l'application du plan terraform sera envoyer, entrer **"yes"** pour continuer
````bash
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes
````

>![alt text](img/image-24.png)
*Sortie console*

7. Vérification du resulatat depuis la console aws
EC2 > Instances > Instances
l'on peut voir qu'il y a effectivement une instance ec2 provisionné de type t3.medium, utilisant l'image défini ainsi que la pair de clé ssh (devops-digital)

>![alt text](img/image-25.png)


## Définition d'un tag pour l'instace ec2 crée
1. Rajout d'un code pour la personnalisation du tag
````bash
    tags = {
        Name = "ec2-aDigital"
    }  
````


2. Relance du terraform plan
````bash
terraform plan
````

>![alt text](img/image-26.png)
L'on observe une replanification des informations sur les ressouces, et la modification locale est prise en compte sans cassé les plan précédement réalisé

3. Application des changements
````bash
terraform apply
````
>![alt text](img/image-27.png)

Seule la modification de tag est opéré au niveau de l'instance qui avait déjà été déployé, il n'y a pas de nouveau déploiement.

4. Vue depuis la console AWS
L'on peut observé que l'instance en cours porté désormais le tag **"ec2-aDigital"**
>![alt text](img/image-28.png)

## Suppression de l'instance ec2
La commande **terraform destroy** Suppression de l'infrastructure précédement provisionnée
````bash
terraform destroy
````

>![alt text](img/image-29.png)
*lancement de la commande "terraform destroy*

>![alt text](img/image-30.png)
*Validation de la suppression de l'infra*

Depuis la console, l'on peut voir que l'instance est bien resiliée
>![alt text](img/image-31.png)
*Instance résiliée*

## Exception lors du "terraform destroy"
pour les images n'embarquant pas la possibilité de detruire simultanément le stockage EBS au moment de la résiliation de l'instace, il faudra rajouter un script au code terraform
````bash
root_block_device {
    delete_on_termination = true
  }
 ````
 ce script est à insérer à la suite du tag.
 Plus d'informations sur cette exection : [doc](https://github.com/hashicorp/terraform-provider-aws/issues/4560)

## Bonus : Terraform fmt
Cette commande permettra de formater le manifest .tf, dont le resultat sera l'alignement des égalités "=" sur la même colone
````bash
terraform fmt
````
>![alt text](img/image-32.png)
*Resultat de la commande "terraform fmt"*

